const AboutUs = () => {
  return (
    <>
      <div className="px-4 py-2 mb-10 mx-auto bg-red-500" id="about-us">
        <div className="flex justify-center mt-8">
          <h1 className="text-yellow-400">About Us</h1>
        </div>
        <div className="flex justify-center mt-4">
          <p className="text-white">
            Anim aute id magna aliqua ad ad non deserunt sunt. Qui irure qui
            lorem cupidatat commodo. Elit sunt amet fugiat veniam occaecat
            fugiat aliqua.
          </p>
        </div>
      </div>
    </>
  );
};

export default AboutUs;
