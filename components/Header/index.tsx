/* This example requires Tailwind CSS v2.0+ */
import { FC, Fragment } from "react";
import { Popover, Transition } from "@headlessui/react";
import { MenuIcon, XIcon } from "@heroicons/react/outline";
import Link from "next/link";
import { NAVBAR } from "../../consts";

interface HeaderProps {
  activeNav: NAVBAR;
}

const Header: FC<HeaderProps> = ({ activeNav }) => {
  return (
    <div className="relative bg-gray-800 overflow-hidden">
      <div className="max-w-7xl mx-auto">
        <div className="relative z-10 pb-3 bg-gray-800 lg:max-w-3xl lg:w-full">
          <Popover>
            <div className="relative pt-6 px-4 sm:px-6 lg:px-8">
              <nav
                className="relative flex items-center justify-between sm:h-10 lg:justify-start"
                aria-label="Global"
              >
                <div className="flex items-center flex-grow flex-shrink-0 lg:flex-grow-0">
                  <div className="flex items-center justify-between w-full md:w-auto">
                    <a href="#">
                      <span className="sr-only">Workflow</span>
                      <img
                        alt="Workflow"
                        className="h-8 w-auto sm:h-10"
                        src="https://tailwindui.com/img/logos/workflow-mark-indigo-600.svg"
                      />
                    </a>
                    <div className="-mr-2 flex items-center md:hidden">
                      <Popover.Button className="bg-gray-800 rounded-md p-2 inline-flex items-center justify-center text-gray-400 hover:text-yellow-400 hover:bg-gray-100 focus:outline-none focus:ring-2 focus:ring-inset focus:ring-indigo-500">
                        <span className="sr-only">Open main menu</span>
                        <MenuIcon className="h-6 w-6" aria-hidden="true" />
                      </Popover.Button>
                    </div>
                  </div>
                </div>
                <div className="hidden md:block md:ml-10 md:pr-4 md:space-x-8">
                  <Link href="/">
                    <a
                      href="#"
                      className={`font-medium ${
                        activeNav === NAVBAR.HOME
                          ? `text-yellow-400`
                          : `text-gray-500`
                      } hover:text-yellow-400`}
                    >
                      Home
                    </a>
                  </Link>
                  <Link href="/#about-us">
                    <a
                      href="#"
                      className={`font-medium ${
                        activeNav === NAVBAR.ABOUTUS
                          ? `text-yellow-400`
                          : `text-gray-500`
                      } hover:text-yellow-400`}
                    >
                      About Us
                    </a>
                  </Link>

                  <Link href="/product-collection">
                    <a
                      className={`font-medium ${
                        activeNav === NAVBAR.PRODUCTCOLLECTION
                          ? `text-yellow-400`
                          : `text-gray-500`
                      } hover:text-yellow-400`}
                    >
                      Product Collections
                    </a>
                  </Link>
                  <Link href="/sizechart">
                    <a
                      className={`font-medium ${
                        activeNav === NAVBAR.SIZECHART
                          ? `text-yellow-400`
                          : `text-gray-500`
                      } hover:text-yellow-400`}
                    >
                      Size Chart
                    </a>
                  </Link>
                </div>
              </nav>
            </div>

            <Transition
              as={Fragment}
              enter="duration-150 ease-out"
              enterFrom="opacity-0 scale-95"
              enterTo="opacity-100 scale-100"
              leave="duration-100 ease-in"
              leaveFrom="opacity-100 scale-100"
              leaveTo="opacity-0 scale-95"
            >
              <Popover.Panel
                focus
                className="absolute z-10 top-0 inset-x-0 p-2 transition transform origin-top-right md:hidden"
              >
                <div className="rounded-lg shadow-md bg-white ring-1 ring-black ring-opacity-5 overflow-hidden">
                  <div className="px-5 pt-4 flex items-center justify-between">
                    <div>
                      <img
                        className="h-8 w-auto"
                        src="https://tailwindui.com/img/logos/workflow-mark-indigo-600.svg"
                        alt=""
                      />
                    </div>
                    <div className="-mr-2">
                      <Popover.Button className="bg-white rounded-md p-2 inline-flex items-center justify-center text-gray-400 hover:text-gray-500 hover:bg-gray-100 focus:outline-none focus:ring-2 focus:ring-inset focus:ring-indigo-500">
                        <span className="sr-only">Close main menu</span>
                        <XIcon className="h-6 w-6" aria-hidden="true" />
                      </Popover.Button>
                    </div>
                  </div>
                  <div className="px-2 pt-2 pb-3 space-y-1 bg-white">
                    <div>
                      <Link href="/sizechart">
                        <a className="font-medium text-gray-500 hover:text-gray-900">
                          Size Chart
                        </a>
                      </Link>
                    </div>
                    <div>
                      <Link href="/sizechart">
                        <a className="font-medium text-gray-500 hover:text-gray-900">
                          Size Chart
                        </a>
                      </Link>
                    </div>
                  </div>
                </div>
              </Popover.Panel>
            </Transition>
          </Popover>
        </div>
      </div>
    </div>
  );
};

export default Header;
