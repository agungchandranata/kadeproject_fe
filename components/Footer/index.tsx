import Image from "next/image";
import Link from "next/link";
import logoInstagram from "../../assets/instagram-logo.png";
import logoWA from "../../assets/whatsapp-logo.png";

const messageWA = "Hai, mau belanja produk ditempatmu dong!";
const urlWA = `https://wa.me/6285716902629/?text=${encodeURIComponent(
  messageWA
)}`;
console.log(urlWA);
const Footer = () => {
  return (
    <>
      <div className="w-full mx-auto p-10 bg-yellow-400 flex flex-col space-y-5">
        <div className="flex w-full justify-center space-x-5">
          <Link href={"/"}>
            <a className="text-white">Home</a>
          </Link>
          <Link href={"/home"} className="text-white">
            About Us
          </Link>
          <Link href={"/home"} className="text-white">
            Product Collections
          </Link>
          <Link href={"/sizechart"} className="text-white">
            Size Chart
          </Link>
        </div>
        <div className="flex w-full justify-center space-x-5">
          <a href="https://www.instagram.com/kadeprojects/" target="_blank">
            <Image width={30} height={30} src={logoInstagram} />
          </a>
          <a href={urlWA} target="_blank">
            <Image width={30} height={30} src={logoWA} />
          </a>
        </div>
        <hr />
        <div className="flex w-full justify-center">
          <p>© Created by AKaBe App</p>
        </div>
      </div>
    </>
  );
};

export default Footer;
