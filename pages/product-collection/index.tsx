import Header from "../../components/Header";
import Image from "next/image";
import mainLogo from "../../assets/mainlogo.jpeg";
import { NAVBAR } from "../../consts";
import Footer from "../../components/Footer";
import "react-responsive-carousel/lib/styles/carousel.min.css";
import React from "react";
import { Carousel } from "react-responsive-carousel";

const ProductCollection = () => {
  const renderProductCollection = () => {
    return (
      <>
        <div className="md:block hidden">
          <div className="flex justify-center">
            <h2 className="text-yellow-400 text-2xl font-bold">Sweater</h2>
          </div>
          <div className="relative max-w-7xl mx-auto">
            <div className="flex space-x-2">
              <div className="xl:grow">
                <Image src={mainLogo} />
              </div>
              <div className="grow">
                <Image src={mainLogo} />
              </div>
              <div className="grow">
                <Image src={mainLogo} />
              </div>
              <div className="grow">
                <Image src={mainLogo} />
              </div>
              <div className="grow">
                <Image src={mainLogo} />
              </div>
            </div>
          </div>
        </div>
      </>
    );
  };

  const tryCarousel = () => {
    return (
      <div className="relative max-w-2xl mx-auto md:hidden">
        <Carousel infiniteLoop autoPlay showIndicators showStatus={false}>
          <Image src={mainLogo} />
          <Image src={mainLogo} />
          <Image src={mainLogo} />
        </Carousel>
      </div>
    );
  };
  return (
    <>
      <Header activeNav={NAVBAR.PRODUCTCOLLECTION} />
      <div className="relative flex justify-center py-2 mb-10">
        <h1 className="text-yellow-400 text-3xl font-bold">
          Product Collection
        </h1>
        <div className="border-2 border-yellow-400 absolute bottom-0 w-24 left-1/2 -translate-x-full" />
      </div>
      {renderProductCollection()}
      {renderProductCollection()}
      {tryCarousel()}
      <Footer />
    </>
  );
};

export default ProductCollection;
