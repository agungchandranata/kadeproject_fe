import type { NextPage } from "next";
import AboutUs from "../components/AboutUs";
import Footer from "../components/Footer";
import Test from "../components/test";

const Home: NextPage = () => {
  return (
    <>
      <Test />
      <AboutUs />
      <Footer />
    </>
  );
};

export default Home;
