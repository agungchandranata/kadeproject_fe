import { Popover, Transition } from "@headlessui/react";
import { MenuIcon, XIcon } from "@heroicons/react/outline";
import Link from "next/link";
import Image from "next/image";
import React, { Fragment } from "react";
import Footer from "../../components/Footer";
import Test from "../../components/test";
import sizeChart from "../../assets/size-chart.jpg";
import Header from "../../components/Header";
import { NAVBAR } from "../../consts";

const SizeChart = () => {
  return (
    <>
      <Header activeNav={NAVBAR.SIZECHART} />
      <div className="relative flex justify-center py-2 mb-2">
        <h1 className="text-yellow-400 text-3xl font-bold">Size Chart</h1>
        <div className="border-2 border-yellow-400 absolute bottom-0 w-24 left-1/2 -translate-x-full" />
      </div>
      <div className="flex justify-center">
        <div className="lg:w-1/3 md:w-1/2">
          <Image src={sizeChart} />
        </div>
      </div>
      <Footer />
    </>
  );
};

export default SizeChart;
